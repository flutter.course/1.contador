import 'package:contador/src/pages/contador.stateful.page.dart';
import 'package:flutter/material.dart';

// Primer widget
class App extends StatelessWidget {
  @override
  Widget build (context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center( 
        child: Contador()
      ),
    );
  }
}