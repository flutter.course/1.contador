import 'package:flutter/material.dart';

class Contador extends StatefulWidget {
  @override
  createState() => _ContadorState();
}

class _ContadorState extends State<Contador> {
  final TextStyle _header = new TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.bold
  );
  final TextStyle _normal = new TextStyle(
    fontSize: 18.0,
  );
  int _conteo = 0;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(
        title: Text('Contador'),
        centerTitle: true
      ),
      body: Column(
        children: [
          Center(
            child: Text(
              'Numero de Clicks:',
              style: _header,
            ),
          ),
          Center(
            child: Text(
              '$_conteo',
              style: _normal,
            ),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      floatingActionButton: _createButtons(),
    );
  }

  Widget _createButtons(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox( width: 25.0,),
        FloatingActionButton(
          onPressed: _changeCount(-1),
          child: Icon(Icons.remove),
        ),
        SizedBox( width: 10.0,),
        FloatingActionButton(
          onPressed: _changeCount(0),
          child: Text('0',style: _header),
        ),
        SizedBox( width: 10.0,),
        FloatingActionButton(
          onPressed: _changeCount(1),
          child: Icon(Icons.add),
        ),
      ],
    );
  }
  Function _changeCount(int direction) {
    return () {
      if (direction != 0) {
        _conteo += direction;
      } else {
        _conteo = direction;
      }
      setState(() {});
    };
    
  }
}