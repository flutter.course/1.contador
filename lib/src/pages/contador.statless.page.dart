import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{
  final TextStyle header = new TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.bold
  );
  final TextStyle normal = new TextStyle(
    fontSize: 18.0,
  );
  final conteo = 10;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(
        title: Text('Contador'),
        centerTitle: true
      ),
      body: Column(
        children: [
          Center(
            child: Text(
              'Numero de Clicks:',
              style: header,
            ),
          ),
          Center(
            child: Text(
              '$conteo',
              style: normal,
            ),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('hola mundo');
          //conteo++;
        },
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      
    );
  }
}